<?php
if(!isset($_SESSION['login'])){
    ?>
    <script>window.location = 'index.php';</script>
    <?php
}
?>


<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.8.0/sweetalert2.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.8.0/sweetalert2.min.js"></script>


<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<style>
    body{
        background: url("img/21.jpg") no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
    h1, label{
        color:#0000A0;
    }
    .artc_logout
    {
        text-align:right;
        padding-right:50px;
        padding-top:10px;
    }
</style>



<div style="background-color: #94DE6E;">
    <div class="artc_header">

        <div class="container">
            <br>
            <div class="artc_logo col-md-3 col-sm-3 col-xs-3">
                <a id="dnn_artc_header_artc_Logo_hypLogo" title="Advanced Remanufacturing and Technology Centre (ARTC)" href="https://www.a-star.edu.sg/artc/"><img class="img-responsive" id="dnn_artc_header_artc_Logo_imgLogo" src="img/artc_new_logo.png" alt="Advanced Remanufacturing and Technology Centre (ARTC)"></a>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-6"></div>
            <div class="col-md-3 col-sm-3 col-xs-3 nanyang_logo" style="text-align:right">

                <a href="http://www.ntu.edu.sg/"><img src="img/nanyang_logo1.png" class="nanyang-img img-responsive"></a>
            </div>


        </div>
    </div>
</div>










<nav class="navbar navbar-inverse" role="navigation" style="font-family: 'Montserrat', sans-serif; background-color: #000000; border-radius: 0px" >
    <div class="container">
        <div class="navbar-header " >
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar_content" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php" id="ddr"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> <strong>HOME</strong></a>
            <a class="navbar-brand navbar-left" href="team.php" id="ddh"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <strong>OUR TEAM</strong></a>
        </div>

        <div class="collapse navbar-collapse navbar-right" id="navbar_content">
            <ul class="nav navbar-nav">
                <?php
                if($_SESSION['login']['role'] == "admin"){
                    ?>
                    <li ><a href="equipments_status.php">Equipments Status</a></li>
                    <li ><a href="pending_users.php">Pending Users</a></li>
                    <li ><a href="add_equipment.php">Add Equipment</a></li>
                    <?php
                }
                ?>
                <li><a href="equipments.php">Equipments</a></li>
                <li><img src="<?php echo $_SESSION['login']['image']; ?>" class="img-responsive img-circle" style="width: 40px; height: 40px; margin-top: 5px"></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['login']['first_name']; ?> <?php echo $_SESSION['login']['last_name']; ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="profile.php"><i class="fa fa-user-circle" aria-hidden="true"></i>
                                Profile</a></li>
                        <li><a href="my_borrows.php"><i class="fa fa-bookmark" aria-hidden="true"></i>
                                My Borrows</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="Validators/logout_validator.php"><i class="fa fa-sign-out" aria-hidden="true"></i>
                                Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
