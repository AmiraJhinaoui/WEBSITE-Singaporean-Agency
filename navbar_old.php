<?php
if(!isset($_SESSION['login'])){
    ?>
    <script>window.location = 'index.php';</script>
    <?php
}
?>


<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<style>
    body{

        background-image:url("img/21.jpg");
        background-repeat:no-repeat;
        background-size:cover;
    }
    h1, label{
        color:#0000A0;
    }
</style>




<nav class="navbar navbar-default ">
    <div class="col-md-8 col-sm-6 col-xs-12" >
        <div class="brand">
            <h1 class="col-md-2"><a id="dnn_Header_LOGO1_hypLogo" title="Advanced Remanufacturing and Technology Centre (ARTC)" href="https://www.a-star.edu.sg/artc/"><img id="dnn_Header_LOGO1_imgLogo" src="img/artc.png"  alt="Advanced Remanufacturing and Technology Centre " /></a></h1>
            <h1 class="col-md-8"><label>Advanced Remanufacturing and Technology Centre</label></h1>
        </div><!--brand end-->
    </div>
</nav>





<nav class="navbar navbar-inverse" role="navigation" style="font-family: 'Montserrat', sans-serif;" >
    <div class="container-fluid">
        <div class="navbar-header " >
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar_content" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php" id="ddr"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> <strong>HOME</strong></a>
            <a class="navbar-brand navbar-left" href="team.php" id="ddh"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <strong>OUR TEAM</strong></a>
        </div>

        <div class="collapse navbar-collapse navbar-right" id="navbar_content">
            <ul class="nav navbar-nav">
                <?php
                if($_SESSION['login']['role'] == "admin"){
                    ?>
                    <li ><a href="equipments_status.php">Equipments Status</a></li>
                    <li ><a href="pending_users.php">Pending Users</a></li>
                    <li ><a href="add_equipment.php">Add Equipment</a></li>
                    <?php
                }
                ?>
                <li><a href="equipments.php">Equipments</a></li>
                <li><img src="img/user.png" class="img-responsive img-circle" style="width: 40px; height: 40px; margin-top: 5px"></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['login']['first_name']; ?> <?php echo $_SESSION['login']['last_name']; ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="profile.php"><i class="fa fa-user-circle" aria-hidden="true"></i>
                                Profile</a></li>
                        <li><a href="my_borrows.php"><i class="fa fa-bookmark" aria-hidden="true"></i>
                                My Borrows</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="Validators/logout_validator.php"><i class="fa fa-sign-out" aria-hidden="true"></i>
                                Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>



















<!--nav class="navbar navbar-default">
    <div class="container-fluid">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Brand</a>
        </div>


        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">
                <?php
                if($_SESSION['login']['role'] == "admin"){
                    ?>
                    <li ><a href="equipments_status.php">Equipments Status</a></li>
                    <li ><a href="pending_users.php">Pending Users</a></li>
                    <li ><a href="add_equipment.php">Add Equipment</a></li>
                <?php
                }
                ?>
                <li><a href="equipments.php">Equipments</a></li>
                <li><img src="img/user.png" class="img-responsive img-circle" style="width: 40px; height: 40px; margin-top: 5px"></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['login']['first_name']; ?> <?php echo $_SESSION['login']['last_name']; ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="profile.php"><i class="fa fa-user-circle" aria-hidden="true"></i>
                                Profile</a></li>
                        <li><a href="my_borrows.php"><i class="fa fa-bookmark" aria-hidden="true"></i>
                                My Borrows</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="Validators/logout_validator.php"><i class="fa fa-sign-out" aria-hidden="true"></i>
                                Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav--!>