<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ahmed JH</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap-select.css">

    <style>
        .thumbnail:hover{
            background-color: black;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->




</head>
<body style=" ">



<?php
include("navbar.php");
?>


<div class="container-fluid">






    <center><h3><strong>Add New Equipment</strong></h3></center>
    <center><p id="error_message" class="text-danger hidden">Error, please check your email and password.</p></center>
    <center><p id="success_message" class="text-success hidden">Equipment successfully added.</p></center>
    <br>


    <div class="row" style="padding: 20px">
        <form method="post" action="Validators/add_equipment_validator.php" enctype="multipart/form-data" >
        <div class="col-md-6">
            <div class="panel panel-primary" style="border-color:black">
                <div class="panel-heading" style="background-color:black; color: white"><strong>Required Info</strong></div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Serial Number *</label>
                        <input type="number" class="form-control"  name="serial_number" placeholder="Serial number" required>
                        <p class="text-danger hidden">Example block-level help text here.</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Name *</label>
                        <input type="text" class="form-control"  name="name" id="name" placeholder="Name" required>
                        <p class="text-danger hidden" id="error_input_name">Name must not contain special caracters.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1"> Image *</label>
                                <br>
                                <label class="custom-file">
                                    <input type="file" name="fileToUpload" id="fileToUpload" class="custom-file-input" required>
                                    <span class="custom-file-control"></span>
                                </label>
                                <p class="text-danger hidden">Example block-level help text here.</p>
                            </div>
                        </div>
                        <div class="col-md-3 pull-right">
                            <img id="img_show" class="img-circle img-responsive" src="img/image.png" style="width: 80px; height: 80px">
                        </div>
                    </div>

                </div>
            </div>
            <button type="submit" class="btn btn-default hidden" id="btn_validate_form">Add</button>
            <button type="button" class="btn btn-primary pull-right" style="width:150px" onclick="validateName($('#name').val())">Add</button>
        </div>

        <div class="col-md-6">
            <div class="panel panel-info" style="border-color:black">
                <div class="panel-heading" style="background-color:black; color: white"><strong>Optional Info</strong></div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Category</label>
                        <input type="text" class="form-control" name="category" placeholder="Category">
                        <p class="text-danger hidden">Example block-level help text here.</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> PO Number</label>
                        <input type="number" class="form-control" name="po_number" placeholder="PO Number">
                        <p class="text-danger hidden">Example block-level help text here.</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> ERP Number</label>
                        <input type="number" class="form-control" name="erp_number" placeholder="ERP Number">
                        <p class="text-danger hidden">Example block-level help text here.</p>
                    </div>
                </div>
            </div>
        </div>



        </form>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/bootstrap-select.js"></script>


<script>
    function isValid(str){
        return !/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(str);
    }

    function validateName(name) {
        if(isValid(name)){
            //alert("valid");
            $('#error_input_name').addClass("hidden");
            $('#btn_validate_form').click();
        }else{
            //alert('NOT valid');
            $('#error_input_name').removeClass("hidden");
        }
    }



    $('#fileToUpload').on('change', function(){
        input = document.getElementById('fileToUpload');
        file = input.files[0];
        console.log(file);

        fr = new FileReader();
        fr.onload = showImage;
        //fr.readAsText(file);
        fr.readAsDataURL(file);

        function showImage() {
            $("#img_show").attr("src",fr.result);
        }
    })
</script>


<?php
if(isset($_SESSION["error"])){
    ?>
    <script>
        $('#error_message').html('<?php echo $_SESSION["error"] ?>');
        $('#error_message').removeClass("hidden");
    </script>
    <?php
    $_SESSION["error"] = null;
}
?>

<?php
if(isset($_SESSION["success"])){
    ?>
    <script>
        $('#success_message').html('<?php echo $_SESSION["success"] ?>');
        $('#success_message').removeClass("hidden");
    </script>
    <?php
    $_SESSION["success"] = null;
}
?>




</body>
</html>