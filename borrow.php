<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ahmed JH</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap-select.css">

    <style>
        .thumbnail:hover{
            background-color: black;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<?php
include("navbar.php");
?>
<div class="container-fluid">






    <?php
    if(!isset($_GET["equipment"])){
        ?>
        <script>window.location = "equipments.php"</script>
        <?php
    }

    require_once('Connection.php');
    require_once('Entities/Equipment.php');
    require_once('Entities/Borrow.php');
    require_once('Entities/User.php');

    $eqp = new Equipment();
    $result = Equipment::findEquipmentById($_GET["equipment"]);
    if($result['status'] == 0){
        ?>
        <script>window.location = 'equipments.php';</script>
        <?php
    }else{
        $eqp = $result['content'];
    }



    ?>

    <div class="row">
        <div class="col-md-4 col-md-offset-4" style="padding: 20px">
            <center><h3><strong>Borrow : <?php echo $eqp->get_name(); ?></strong></h3></center>
            <div class="row">
                <div class="col-md-6">
                    <img src="<?php echo $eqp->get_image() ?>" class="img-circle" alt="..." style="width:150px;height:150px; ">
                </div>
                <div class="col-md-6">
                    <h5><strong>Serial Number : <?php echo $eqp->get_serial_number(); ?></strong></h5>
                    <h5><strong>PO Number : <?php if($eqp->get_po_number() != null){echo $eqp->get_po_number();}else{ echo "-----";} ?></strong></h5>
                    <h5><strong>ERP Number : <?php if($eqp->get_erp_number() != null){echo $eqp->get_erp_number();}else{ echo "-----";} ?></strong></h5>
                    <h5><strong>Category : <?php if($eqp->get_category() != null){echo $eqp->get_category();}else{ echo "-----";} ?></strong></h5>
                </div>
            </div>
            <form method="post" action="Validators/borrow_validator.php">



                <br>
                <br>

                <h4><strong>Choose date of borrow and retrieve : </strong></h4>

                <div id="date" class="input-group input-daterange" data-provide="datepicker">
                    <div class="input-group-addon">
                        <span class="fa fa-calendar"> Date borrow</span>
                    </div>
                    <input style="width: 150px" type="text" class="form-control" name="date_borrow" value="<?php echo Date("m/d/Y"); ?>" readonly required>


                    <div class="input-group-addon">
                        <span class="fa fa-calendar"> Date retrieve</span>
                    </div>
                    <input style="width: 150px" type="text" class="form-control" name="date_retrieve" value="<?php echo Date("m/d/Y"); ?>" readonly required>

                </div>

                <input type="hidden" name="eq_serial_number" value="<?php echo $eqp->get_serial_number(); ?>">

                <br>
                <br>




                <!--div-- class="input-group date" data-provide="datepicker">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div-->




                <button type="submit" class="btn btn-default">Borrow</button>
            </form>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/bootstrap-select.js"></script>


<script>
    var datesToDisable = [];
</script>

<?php
$borrows = Borrow::getBorrowDetails($_GET["equipment"]);
if($borrows['status'] == 1){
    foreach ($borrows['content'] as $one){
        /*$dtBorrow = date_create($one->get_date_borrow());
        $dtRetrieve = date_create($one->get_date_retrieve());
        $dtBorrowFormatted = date_format($dtBorrow,"m/d/Y");
        $dtRetrieveFormatted = date_format($dtRetrieve,"m/d/Y");
        echo $dtBorrowFormatted;
        echo '<br>';
        echo $dtRetrieveFormatted;*/

        if($one->get_date_retrieve_check() == 0){
    $dtRetrieve = date_create($one->get_date_retrieve());
    $dtRetrieveFormatted = date_format($dtRetrieve,"m/d/Y");

    $period = new DatePeriod(
        new DateTime($one->get_date_borrow()),
        new DateInterval('P1D'),
        new DateTime($one->get_date_retrieve())
    );
    ?>
    <script>
        datesToDisable.push("<?php echo $dtRetrieveFormatted ?>");
        //datesToDisable = ["<?php echo $dtRetrieveFormatted ?>"];
        <?php
        foreach($period as $date){
        //echo $date->format("m/d/Y") . "<br>";
        ?>
        datesToDisable.push('<?php echo $date->format("m/d/Y") ?>');
        <?php
        }
        ?>

    </script>
<?php
        }
    }
    ?>
        <script>
            $('#date').datepicker({
                startDate: "<?php echo Date("m/d/Y"); ?>",
                todayHighlight: true,
                datesDisabled: datesToDisable
            });
        </script>
    <?php
}else{
    ?>
    <script>
        //var datesToDisable = ["08/25/2017","08/27/2017"];
        $('#date').datepicker({
            startDate: "<?php echo Date("m/d/Y"); ?>",
            todayHighlight: true
            //datesDisabled: datesToDisable
        });
    </script>
    <?php
}
?>




</body>
</html>