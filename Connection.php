<?php
class Connection {
    private static $instance = NULL;
    private static $host = "localhost";
    private static $dbName = "ahmed_jh";
    private static $username = "root";
    private static $password = "";

    private function __construct() {}

    private function __clone() {}

    public static function getInstance() {
        if (!isset(self::$instance)) {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            self::$instance = new PDO('mysql:host='.Connection::$host.';dbname='.Connection::$dbName, Connection::$username, Connection::$password, $pdo_options);
        }
        return self::$instance;
    }
}
?>