<?php
session_start();
if(isset($_POST["email"])) {
    require_once('../Connection.php');
    require_once('../Entities/User.php');

    $res = User::checkUserPassword($_POST['user_id'],$_POST['password']);
    if($res == 1){
        $result = User::changeUserEmail($_POST['user_id'],$_POST['email']);
        if($result == 1){
            $_SESSION['success'] = "Success, your email has been successfully changed";
            $_SESSION['login']['email'] = $_POST['email'];
        }else{
            echo $result;
            $_SESSION['error'] = "Error, something went wrong";
        }
    }else{
        $_SESSION['error'] = "Error, wrong password.";
    }


}

?>


<script>
    window.location = '../profile.php';
</script>

