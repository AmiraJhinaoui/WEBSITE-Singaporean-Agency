<?php
session_start();
if(isset($_POST["user_id"])){
    require_once('../Connection.php');
    require_once('../Entities/User.php');

    $result = 0;

    var_dump($_FILES["change_img_file"]);


    $target_dir = "../img/profile/";
    //$target_file = $target_dir . basename($_FILES["change_img_file"]["name"]);
    $target_file = $target_dir . $_POST["user_id"] . "." . pathinfo($_FILES["change_img_file"]["name"],PATHINFO_EXTENSION);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    $check = getimagesize($_FILES["change_img_file"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $_SESSION['error'] = "File is not an image.";
        $uploadOk = 0;
    }


    // Check file size
    if ($_FILES["change_img_file"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $_SESSION['error'] = "Sorry, your file is too large.";
        $uploadOk = 0;
    }

    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $_SESSION['error'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
    } else {
        // Check if file already exists
        if (file_exists($target_file)) {
            unlink($target_file);
        }
        if (move_uploaded_file($_FILES["change_img_file"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["change_img_file"]["name"]). " has been uploaded.";
            $result = User::changeUserImage($_POST['user_id'],"img/profile/" . $_POST["user_id"] . "." . pathinfo($_FILES["change_img_file"]["name"],PATHINFO_EXTENSION));
            echo $result;
            if($result == 1){
                $_SESSION['success'] = "Success, Profile image successfully updated";
                $_SESSION['login']['image'] = "img/profile/" . $_POST["user_id"] . "." . pathinfo($_FILES["change_img_file"]["name"],PATHINFO_EXTENSION);
            }else{
                $_SESSION['error'] = "Error, something went wrong";
            }
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
}

?>

<script>
    window.location = '../profile.php';
</script>
