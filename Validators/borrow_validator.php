<?php
session_start();
if(isset($_POST["date_borrow"])) {
    require_once('../Connection.php');
    require_once('../Entities/Equipment.php');
    require_once('../Entities/Borrow.php');
    $result = 0;

    $date_borrow = date_create($_POST['date_borrow']);
    $date_retrieve = date_create($_POST['date_retrieve']);

    $borrow = new Borrow();
    $borrow->set_equipment_id($_POST['eq_serial_number']);
    $borrow->set_borrower_id($_SESSION['login']['id']);
    $borrow->set_date_borrow(date_format($date_borrow,"Y-m-d"));
    $borrow->set_date_retrieve(date_format($date_retrieve,"Y-m-d"));
    $borrow->set_retrieved(0);


    //echo date_format($date,"Y-m-d");

    $result = Borrow::addBorrow($borrow);
    echo $result;
    if($result == 1){
        $_SESSION['success'] = "Success, Equipment successfully borrowed";
    }else{
        $_SESSION['error'] = "Error, something went wrong";
    }
}

?>


<script>
    window.location = '../equipments.php';
</script>

