<?php
session_start();
if(isset($_POST["email"])){
    require_once('../Connection.php');
    require_once('../Entities/User.php');
    $result = User::login($_POST["email"],$_POST["password"]);
    if($result["status"] == 1){
        $_SESSION['login']['id'] = $result["content"]->get_id();
        $_SESSION['login']['first_name'] = $result["content"]->get_first_name();
        $_SESSION['login']['last_name'] = $result["content"]->get_last_name();
        $_SESSION['login']['email'] = $result["content"]->get_email();
        $_SESSION['login']['role'] = $result["content"]->get_role();
        $_SESSION['login']['image'] = $result["content"]->get_image();
    }else{
        echo $result["status"];
        $_SESSION['error'] = $result["content"];
    }

}

?>


<script>
    window.location = '../index.php';
</script>
