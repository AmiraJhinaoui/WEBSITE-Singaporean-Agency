<?php
session_start();
if(isset($_POST["first_name"])) {
    require_once('../Connection.php');
    require_once('../Entities/User.php');

    $res = User::checkUserPassword($_POST['user_id'],$_POST['password']);
    if($res == 1){
        $result = User::changeUserName($_POST['user_id'],$_POST['first_name'],$_POST['last_name']);
        if($result == 1){
            $_SESSION['success'] = "Success, your name has been successfully changed";
            $_SESSION['login']['first_name'] = $_POST['first_name'];
            $_SESSION['login']['last_name'] = $_POST['last_name'];
        }else{
            echo $result;
            $_SESSION['error'] = "Error, something went wrong";
        }
    }else{
        $_SESSION['error'] = "Error, wrong password.";
    }


}

?>


<script>
    window.location = '../profile.php';
</script>

