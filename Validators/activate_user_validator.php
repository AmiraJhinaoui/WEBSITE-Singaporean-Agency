<?php
session_start();
if(isset($_POST["user_id_activate"])){
    require_once('../Connection.php');
    require_once('../Entities/User.php');
    //echo $_POST['user_id_activate'];
    if($_POST['user_id_activate'] != 0){
        $result = User::activateUser($_POST['user_id_activate']);
        if($result == 1){
            $_SESSION['success'] = 'Success, User has been activated.';
        }else{
            $_SESSION['error'] = 'Error, Something went wrong.';
        }
    }else{
        $_SESSION['error'] = 'Error, Something went wrong.';
    }
}

?>


<script>
    window.location = '../pending_users.php';
</script>
