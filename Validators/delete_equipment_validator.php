<?php
session_start();
require_once('../Connection.php');
require_once('../Entities/Equipment.php');
require_once('../Entities/Borrow.php');
$serial = $_POST['eq_serial'];
$img = $_POST['eq_img'];

$result = Equipment::deleteEquipment($serial);
if($result == 1){
    $result2 = Borrow::deleteBorrowsOfEquipment($serial);
    $result3 = Equipment::deleteImage($img);

    $_SESSION['success'] = "Success, Equipment successfully deleted";
}else{
    $_SESSION['error'] = "Error, something went wrong";
}
?>

<script>
    window.location = '../equipments.php';
</script>
