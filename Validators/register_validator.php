<?php
session_start();
if(isset($_POST["fName"])){
    require_once('../Connection.php');
    require_once('../Entities/User.php');

    $confirmPass = $_POST['confirm_password'];
    $pass = $_POST["password"];
    if($pass == $confirmPass){
        if(strlen($pass) < 8){
            $_SESSION['error'] = "Error, password must at least contain 8 caracters.";
            ?>
            <script>
                window.location = '../register.php';
            </script>
            <?php
        }else{
            $user = new User();
            $user->set_first_name($_POST["fName"]);
            $user->set_last_name($_POST["lName"]);
            $user->set_email($_POST["email"]);
            $user->set_password($_POST["password"]);
            $user->set_role("member");
            $result = User::addUser($user);

            if($result["status"] != 1){
                //$_SESSION['error'] = "Error, something went wrong.";
                $_SESSION['error'] = $result["content"];
                ?>
                <script>
                    window.location = '../register.php';
                </script>
                <?php
            }else{
                $_SESSION['success'] = "Success, your request of creating an account will be handled by admin";
                ?>
                <script>
                    window.location = '../index.php';
                </script>
                <?php
            }
        }
    }else{
        $_SESSION['error'] = "Error, password does not match.";
        ?>
        <script>
            window.location = '../register.php';
        </script>
        <?php
    }
}

?>


