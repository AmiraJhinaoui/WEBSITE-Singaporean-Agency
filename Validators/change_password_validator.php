<?php
session_start();
if(isset($_POST["old_password"])) {
    require_once('../Connection.php');
    require_once('../Entities/User.php');

    if(strlen($_POST['new_password']) < 8){
        $_SESSION['error'] = "Error, password must at least contain 8 caracters.";
    }else{
        $res = User::checkUserPassword($_POST['user_id'],$_POST['old_password']);
        if($res == 1){
            $result = User::changeUserPassword($_POST['user_id'],$_POST['new_password']);
            if($result == 1){
                $_SESSION['success'] = "Success, your password has been successfully changed";
            }else{
                $_SESSION['error'] = "Error, something went wrong";
            }
        }else{
            $_SESSION['error'] = "Error, wrong password.";
        }
    }
}

?>


<script>
    window.location = '../profile.php';
</script>

