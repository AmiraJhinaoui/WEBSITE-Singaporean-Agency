<?php

class Equipment{

    private $serial_number;
    private $name;
    private $category;
    private $image;
    private $po_number;
    private $erp_number;
    private $taken;

    public function __construct() {

    }

    public function get_serial_number()
    {
        return $this->serial_number;
    }
    public function set_serial_number($serial_number)
    {
        return $this->serial_number = $serial_number;
    }

    public function get_name()
    {
        return $this->name;
    }
    public function set_name($name)
    {
        $this->name = $name;
    }

    public function get_category()
    {
        return $this->category;
    }
    public function set_category($categ)
    {
        $this->category = $categ;
    }

    public function get_image()
    {
        return $this->image;
    }
    public function set_image($img)
    {
        return $this->image = $img;
    }

    public function get_po_number()
    {
        return $this->po_number;
    }
    public function set_po_number($po_number)
    {
        $this->po_number = $po_number;
    }

    public function get_erp_number()
    {
        return $this->erp_number;
    }
    public function set_erp_number($erp_number)
    {
        $this->erp_number = $erp_number;
    }

    public function get_taken()
    {
        return $this->taken;
    }
    public function set_taken($taken)
    {
        $this->taken = $taken;
    }


    public static function addEquipment($newEquipment) //RETURN: 1 IF SUCCESS, ERROR MESSAGE IF NOT
    {
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("INSERT INTO equipments (Serial_number,name,category,image,taken,PO_number,eRP_number) VALUES (:serial_number,:name,:category,:image,:taken,:po_number,:erp_number)");
            $req->execute(array(
                'serial_number' => $newEquipment->get_serial_number(),
                'name' => $newEquipment->get_name(),
                'category' => $newEquipment->get_category(),
                'image' => $newEquipment->get_image(),
                'taken' => $newEquipment->get_taken(),
                'po_number' => $newEquipment->get_po_number(),
                'erp_number' => $newEquipment->get_erp_number()
            ));
            //return $db->lastInsertId();
            return 1;
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    public static function updateEquipment($newEquipment,$oldSerialNumber) //RETURN: 1 IF SUCCESS, ERROR MESSAGE IF NOT
    {
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("UPDATE equipments SET Serial_number=:serial_number, name=:name, category=:category, image=:image, taken=:taken, PO_number=:po_number, eRP_number=:erp_number WHERE Serial_number=:oldSnum");
            $req->execute(array(
                'serial_number' => $newEquipment->get_serial_number(),
                'name' => $newEquipment->get_name(),
                'category' => $newEquipment->get_category(),
                'image' => $newEquipment->get_image(),
                'taken' => $newEquipment->get_taken(),
                'po_number' => $newEquipment->get_po_number(),
                'erp_number' => $newEquipment->get_erp_number(),
                'oldSnum' => $oldSerialNumber
            ));
            //return $db->lastInsertId();
            return 1;
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    public static function findEquipmentById($id) // RETURN: 0 IF NO USER FOUND, ERROR MESSAGE IF ERROR, USER IF SUCCESS
    {
        $result = array();
        $result["status"] = 0;
        $result["content"] = null;
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("SELECT * FROM equipments WHERE Serial_number=:id");
            $req->execute(array(
                'id' => $id
            ));
            if($req->rowCount() == 0){
                $result["status"] = 0;
                return $result;
            }else{
                $eqp = new Equipment();
                foreach ($req->fetchAll() as $eq) {
                    $eqp->set_serial_number($eq['Serial_number']);
                    $eqp->set_name($eq['name']);
                    $eqp->set_category($eq['category']);
                    $eqp->set_image($eq['image']);
                    $eqp->set_taken($eq['taken']);
                    $eqp->set_po_number($eq['PO_number']);
                    $eqp->set_erp_number($eq['eRP_number']);
                }
                $result["status"] = 1;
                $result["content"] = $eqp;
                return $result;
            }
        }catch(Exception $e){
            $result["status"] = 0;
            $result["content"] = $e->getMessage();
            return $result;
        }
    }

    public static function findEquipmentByName($name) // RETURN: 0 IF NO USER FOUND, ERROR MESSAGE IF ERROR, USER IF SUCCESS
    {
        $result = array();
        $result["status"] = 0;
        $result["content"] = null;
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("SELECT * FROM equipments WHERE name=:name");
            $req->execute(array(
                'name' => $name
            ));
            if($req->rowCount() == 0){
                $result["status"] = 0;
                return $result;
            }else{
                $eqp = new Equipment();
                foreach ($req->fetchAll() as $eq) {
                    $eqp->set_serial_number($eq['Serial_number']);
                    $eqp->set_name($eq['name']);
                    $eqp->set_category($eq['category']);
                    $eqp->set_image($eq['image']);
                    $eqp->set_taken($eq['taken']);
                    $eqp->set_po_number($eq['PO_number']);
                    $eqp->set_erp_number($eq['eRP_number']);
                }
                $result["status"] = 1;
                $result["content"] = $eqp;
                return $result;
            }
        }catch(Exception $e){
            $result["status"] = 0;
            $result["content"] = $e->getMessage();
            return $result;
        }
    }


    public static function getAllEquipments() //RETURN: 0 IF NOTHING FOUND, LIST OF EQUIPMENTS IF SUCCESS; ERROR MESSAGE IF ERROR
    {
        $result = array();
        $result["status"] = 0;
        $result["content"] = null;
        try{
            $db = Connection::getInstance();
            $req = $db->query("SELECT * FROM equipments");
            $list = array();
            if($req->rowCount() == 0){
                $result["status"] = 0;
                $result["content"] = 'Nothing found';
                return $result;
            }else{
                foreach ($req->fetchAll() as $eq) {
                    $eqp = new Equipment();
                    $eqp->set_serial_number($eq['Serial_number']);
                    $eqp->set_name($eq['name']);
                    $eqp->set_category($eq['category']);
                    $eqp->set_image($eq['image']);
                    $eqp->set_taken($eq['taken']);
                    $eqp->set_po_number($eq['PO_number']);
                    $eqp->set_erp_number($eq['eRP_number']);
                    array_push($list,$eqp);
                }
                $result["status"] = 1;
                $result["content"] = $list;
                return $result;
            }
        }catch(Exception $e){
            $result["status"] = 0;
            $result["content"] = $e->getMessage();
            return $result;
        }
    }

    public static function getAllEquipmentsPagination($start,$offset) //RETURN: 0 IF NOTHING FOUND, LIST OF EQUIPMENTS IF SUCCESS; ERROR MESSAGE IF ERROR
    {
        $result = array();
        $result["status"] = 0;
        $result["content"] = null;
        try{
            $db = Connection::getInstance();
            $req = $db->query("SELECT * FROM equipments ORDER BY Serial_number LIMIT " . $start . ", " . $offset);
            $list = array();
            if($req->rowCount() == 0){
                $result["status"] = 0;
                $result["content"] = 'Nothing found';
                return $result;
            }else{
                foreach ($req->fetchAll() as $eq) {
                    $eqp = new Equipment();
                    $eqp->set_serial_number($eq['Serial_number']);
                    $eqp->set_name($eq['name']);
                    $eqp->set_category($eq['category']);
                    $eqp->set_image($eq['image']);
                    $eqp->set_taken($eq['taken']);
                    $eqp->set_po_number($eq['PO_number']);
                    $eqp->set_erp_number($eq['eRP_number']);
                    array_push($list,$eqp);
                }
                $result["status"] = 1;
                $result["count"] = Equipment::getCountEquipments();
                $result["content"] = $list;
                return $result;
            }
        }catch(Exception $e){
            $result["status"] = 0;
            $result["content"] = $e->getMessage();
            return $result;
        }
    }

    public static function getCountEquipments()
    {
        $result = 0;
        try{
            $db = Connection::getInstance();
            $req = $db->query("SELECT * FROM equipments");
            if($req->rowCount() == 0){
                $result = 0;
                return $result;
            }else{
                $result = $req->rowCount();
                return $result;
            }
        }catch(Exception $e){
            $result = 0;
            return $result;
        }
    }

    public static function deleteEquipment($equipmentId)
    {
        try{
            $db = Connection::getInstance();
            $req = $db->query("DELETE FROM equipments WHERE Serial_number=".$equipmentId);
            return 1;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    public static function deleteImage($imgPath)
    {
        $filename = "../".$imgPath;
        if(file_exists($filename)) {
            unlink($filename);
            return 1;
        } else {
            return 0;
        }
    }

    public static function updateEquipmentStatus($equipmentId,$taken)
    {
        try{
            $db = Connection::getInstance();
            $req = $db->query("UPDATE equipments SET taken=".$taken." WHERE Serial_number=".$equipmentId);
            return 1;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }


}
?>