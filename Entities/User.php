<?php

class User{

    private $id;
    private $firstName;
    private $lastName;
    private $email;
    private $password;
    private $role;
    private $approved;
    private $image;

    public function __construct()
    {
        $this->email = "lll@ff";
    }

    public function get_id()
    {
        return $this->id;
    }
    public function set_id($id)
    {
        $this->id = $id;
    }

    public function get_first_name()
    {
        return $this->firstName;
    }
    public function set_first_name($fName)
    {
        $this->firstName = $fName;
    }

    public function get_last_name()
    {
        return $this->lastName;
    }
    public function set_last_name($lName)
    {
        $this->lastName = $lName;
    }

    public function get_email()
    {
        return $this->email;
    }
    public function set_email($email)
    {
        $this->email = $email;
    }

    public function get_password()
    {
        return $this->password;
    }
    public function set_password($pass)
    {
        $this->password = $pass;
    }

    public function get_role()
    {
        return $this->role;
    }
    public function set_role($role)
    {
        $this->role = $role;
    }

    public function get_approved()
    {
        return $this->approved;
    }
    public function set_approved($approved)
    {
        $this->approved = $approved;
    }

    public function get_image()
    {
        return $this->image;
    }
    public function set_image($img)
    {
        $this->image = $img;
    }

    public static function addUser($newUser) //RETURN: 1 IF SUCCESS, ERROR MESSAGE IF NOT
    {
        $result = array();
        $result["status"] = 0;
        $result["content"] = null;
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("INSERT INTO users (first_name,last_name,email,password,role,approved) VALUES (:fName,:lName,:email,:pass,:role,:approved)");
            $req->execute(array(
                'fName' => $newUser->get_first_name(),
                'lName' => $newUser->get_last_name(),
                'email' => $newUser->get_email(),
                'pass' => $newUser->get_password(),
                'role' => $newUser->get_role(),
                'approved' => 0
            ));
            $result["status"] = 1;
            return $result;
        }catch(Exception $e){
            $result["status"] = 0;
            if($e->getCode() == 23000){
                $result["content"] = "Error, an account with the email '" . $newUser->get_email() . "' already exist.";
            }else{
                $result["content"] = $e->getMessage();
            }
            return $result;
        }
    }

    public static function findUserById($id) // RETURN: 0 IF NO USER FOUND, ERROR MESSAGE IF ERROR, USER IF SUCCESS
    {
        $result = array();
        $result["status"] = 0;
        $result["content"] = null;
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("SELECT * FROM users WHERE id=:id");
            $req->execute(array(
                'id' => $id
            ));
            if($req->rowCount() == 0){
                $result["status"] = 0;
                $result["content"] = 'no users found';
                return $result;
            }else{
                $user = new User();
                foreach ($req->fetchAll() as $usr) {
                    $user->set_id($usr['id']);
                    $user->set_first_name($usr['first_name']);
                    $user->set_last_name($usr['last_name']);
                    $user->set_email($usr['email']);
                    $user->set_password($usr['password']);
                    $user->set_role($usr['role']);
                    $user->set_approved($usr['approved']);
                    $user->set_image($usr['image']);
                }
                $result["status"] = 1;
                $result["content"] = $user;
                return $result;
            }
        }catch(Exception $e){
            $result["status"] = 0;
            $result["content"] = $e->getMessage();
            return $result;
        }
    }

    public static function getAllUsers() //RETURN: 0 IF NOTHING FOUND, LIST OF USERS IF SUCCESS; ERROR MESSAGE IF ERROR
    {
        $result = array();
        $result["status"] = 0;
        $result["content"] = null;
        try{
            $db = Connection::getInstance();
            $req = $db->query("SELECT * FROM users");
            $list = array();
            if($req->rowCount() == 0){
                $result["status"] = 0;
                $result["content"] = 'nothing found';
                return $result;
            }else{
                foreach ($req->fetchAll() as $usr) {
                    $user = new User();
                    $user->set_id($usr['id']);
                    $user->set_first_name($usr['first_name']);
                    $user->set_last_name($usr['last_name']);
                    $user->set_email($usr['email']);
                    $user->set_password($usr['password']);
                    $user->set_role($usr['role']);
                    $user->set_approved($usr['approved']);
                    $user->set_image($usr['image']);
                    array_push($list,$user);
                }
                $result["status"] = 1;
                $result["content"] = $list;
                return $result;
            }
        }catch(Exception $e){
            $result["status"] = 0;
            $result["content"] = $e->getMessage();
            return $result;
        }
    }

    public static function getPendingUsers() //RETURN: 0 IF NOTHING FOUND, LIST OF USERS IF SUCCESS; ERROR MESSAGE IF ERROR
    {
        $result = array();
        $result["status"] = 0;
        $result["content"] = null;
        try{
            $db = Connection::getInstance();
            $req = $db->query("SELECT * FROM users WHERE approved=0");
            $list = array();
            if($req->rowCount() == 0){
                $result["status"] = 0;
                $result["content"] = 'Nothing Found';
                return $result;
            }else{
                foreach ($req->fetchAll() as $usr) {
                    $user = new User();
                    $user->set_id($usr['id']);
                    $user->set_first_name($usr['first_name']);
                    $user->set_last_name($usr['last_name']);
                    $user->set_email($usr['email']);
                    $user->set_password($usr['password']);
                    $user->set_role($usr['role']);
                    $user->set_approved($usr['approved']);
                    $user->set_image($usr['image']);
                    array_push($list,$user);
                }
                $result["status"] = 1;
                $result["content"] = $list;
                return $result;
            }
        }catch(Exception $e){
            $result["status"] = 0;
            $result["content"] = $e->getMessage();
            return $result;
        }
    }

    public static function deleteUser($userId)
    {
        try{
            $db = Connection::getInstance();
            $req = $db->query("DELETE FROM users WHERE id=".$userId);
            return 1;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    public static function login($email,$password)
    {
        $result = array();
        $result["status"] = 0;
        $result["content"] = null;
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("SELECT * FROM users WHERE email=:email AND password=:password");
            $req->execute(array(
                'email' => $email,
                'password' => $password
            ));
            if($req->rowCount() == 0){
                $result["status"] = 0;
                $result["content"] = 'Error, please check your email and password.';
                return $result;
            }else{
                $user = new User();
                foreach ($req->fetchAll() as $usr) {
                    $user->set_id($usr['id']);
                    $user->set_first_name($usr['first_name']);
                    $user->set_last_name($usr['last_name']);
                    $user->set_email($usr['email']);
                    $user->set_password($usr['password']);
                    $user->set_role($usr['role']);
                    $user->set_approved($usr['approved']);
                    $user->set_image($usr['image']);
                }
                if($user->get_approved() == 0){
                    $result["status"] = 0;
                    $result["content"] = 'Error, user is not approved yet';
                    return $result;
                }else{
                    $result["status"] = 1;
                    $result["content"] = $user;
                    return $result;
                }
            }
        }catch (Exception $e){
            $result["status"] = 0;
            $result["content"] = $e->getMessage();
            return $result;
        }
    }

    public static function activateUser($userId)
    {
        try{
            $db = Connection::getInstance();
            $req = $db->query("UPDATE users SET approved=1 WHERE id=".$userId);
            return 1;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    public static function changeUserName($userId,$fName,$lName)
    {
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("UPDATE users SET first_name=:fName, last_name=:lName WHERE id=:id");
            $req->execute(array(
                'id' => $userId,
                'fName' => $fName,
                'lName' => $lName
            ));
            return 1;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    public static function changeUserEmail($userId,$email)
    {
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("UPDATE users SET email=:email WHERE id=:id");
            $req->execute(array(
                'id' => $userId,
                'email' => $email
            ));
            return 1;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    public static function changeUserPassword($userId,$pass)
    {
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("UPDATE users SET password=:pass WHERE id=:id");
            $req->execute(array(
                'id' => $userId,
                'pass' => $pass
            ));
            return 1;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    public static function checkUserPassword($userId,$password)
    {
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("SELECT * FROM users WHERE id=:id AND password=:password");
            $req->execute(array(
                'id' => $userId,
                'password' => $password
            ));
            if($req->rowCount() == 0){
                return 0;
            }else{
                return 1;
            }
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    public static function changeUserImage($userId,$image)
    {
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("UPDATE users SET image=:image WHERE id=:id");
            $req->execute(array(
                'id' => $userId,
                'image' => $image
            ));
            return 1;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }


}
?>