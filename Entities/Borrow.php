<?php

class Borrow{

    private $id;
    private $equipment_id;
    private $borrower_id;
    private $equipmentName;
    private $equipmentImage;
    private $borrowerFirstName;
    private $borrowerLastName;
    private $dateBorrow;
    private $dateRetrieve;
    private $dateBorrowCheck;
    private $dateRetrieveCheck;
    private $retrieved;

    public function __construct() {

    }

    public function get_id()
    {
        return $this->id;
    }
    public function set_id($id)
    {
        return $this->id = $id;
    }

    public function get_equipment_id()
    {
        return $this->equipment_id;
    }
    public function set_equipment_id($eqp_id)
    {
        return $this->equipment_id = $eqp_id;
    }

    public function get_borrower_id()
    {
        return $this->borrower_id;
    }
    public function set_borrower_id($borrower_id)
    {
        return $this->borrower_id = $borrower_id;
    }

    public function get_equipment_name()
    {
        return $this->equipmentName;
    }
    public function set_equipment_name($eqName)
    {
        return $this->equipmentName = $eqName;
    }

    public function get_equipment_image()
    {
        return $this->equipmentImage;
    }
    public function set_equipment_image($eqImage)
    {
        return $this->equipmentImage = $eqImage;
    }

    public function get_borrower_first_name()
    {
        return $this->borrowerFirstName;
    }
    public function set_borrower_first_name($fName)
    {
        $this->borrowerFirstName = $fName;
    }

    public function get_borrower_last_name()
    {
        return $this->borrowerLastName;
    }
    public function set_borrower_last_name($lName)
    {
        $this->borrowerLastName = $lName;
    }

    public function get_date_borrow()
    {
        return $this->dateBorrow;
    }
    public function set_date_borrow($borrowDate)
    {
        return $this->dateBorrow = $borrowDate;
    }

    public function get_date_retrieve()
    {
        return $this->dateRetrieve;
    }
    public function set_date_retrieve($retrieveDate)
    {
        return $this->dateRetrieve = $retrieveDate;
    }

    public function get_date_borrow_check()
    {
        return $this->dateBorrowCheck;
    }
    public function set_date_borrow_check($dtBorrowCheck)
    {
        return $this->dateBorrowCheck = $dtBorrowCheck;
    }

    public function get_date_retrieve_check()
    {
        return $this->dateRetrieveCheck;
    }
    public function set_date_retrieve_check($dtRetrieveCheck)
    {
        return $this->dateRetrieveCheck = $dtRetrieveCheck;
    }

    public function get_retrieved()
    {
        return $this->retrieved;
    }
    public function set_retrieved($retrieved)
    {
        return $this->retrieved = $retrieved;
    }


    public static function getBorrowDetails($equipmentId) // RETURN: 0 IF NO USER FOUND, ERROR MESSAGE IF ERROR, USER IF SUCCESS
    {
        $result = array();
        $result["status"] = 0;
        $result["content"] = null;
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("SELECT * FROM borrows WHERE equipment_id =:id AND retrieved=0 ORDER BY date_submitted DESC");
            $req->execute(array(
                'id' => $equipmentId
            ));
            $list = array();
            if($req->rowCount() == 0){
                $result["status"] = 0;
                $result["content"] = 'Nothing found.';
                return $result;
            }else{

                foreach ($req->fetchAll() as $br) {
                    $borrow = new Borrow();
                    $borrow->set_id($br['id']);
                    $borrow->set_date_borrow($br['date_borrow']);
                    $borrow->set_date_retrieve($br['date_retrieve']);
                    $borrow->set_date_borrow_check($br['date_borrow_check']);
                    $borrow->set_date_retrieve_check($br['date_retrieve_check']);
                    $eq = Equipment::findEquipmentById($equipmentId)["content"];
                    $borrow->set_equipment_name($eq->get_name());
                    $borrow->set_equipment_image($eq->get_image());
                    $user = User::findUserById($br['user_id'])["content"];
                    $borrow->set_borrower_first_name($user->get_first_name());
                    $borrow->set_borrower_last_name($user->get_last_name());
                    $borrow->set_retrieved($br['retrieved']);
                    $borrow->set_equipment_id($br['equipment_id']);
                    $borrow->set_borrower_id($br['user_id']);
                    array_push($list,$borrow);
                }
                $result["status"] = 1;
                $result["content"] = $list;
                return $result;
            }
        }catch(Exception $e){
            $result["status"] = 0;
            $result["content"] = $e->getMessage();
            return $result;
        }
    }

    public static function addBorrow($newBorrow) //RETURN: 1 IF SUCCESS, ERROR MESSAGE IF NOT
    {
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("INSERT INTO borrows (user_id,equipment_id,date_borrow,date_retrieve,retrieved) VALUES (:user_id,:equipment_id,:date_borrow,:date_retrieve,:retrieved)");
            $req->execute(array(
                'user_id' => $newBorrow->get_borrower_id(),
                'equipment_id' => $newBorrow->get_equipment_id(),
                'date_borrow' => $newBorrow->get_date_borrow(),
                'date_retrieve' => $newBorrow->get_date_retrieve(),
                'retrieved' => $newBorrow->get_retrieved()
            ));
            //return $db->lastInsertId();
            //return Equipment::updateEquipmentStatus($newBorrow->get_equipment_id(),1);
            return 1;
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    public static function deleteBorrowsOfEquipment($eqSerial) //RETURN: 1 IF SUCCESS, ERROR MESSAGE IF NOT
    {
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("DELETE FROM borrows WHERE equipment_id=:eqSerial");
            $req->execute(array(
                'eqSerial' => $eqSerial
            ));
            return 1;
        }catch(Exception $e){
            return $e->getMessage();
        }
    }


    public static function getUserBorrows($userId,$equipmentId) // RETURN: 0 IF NO USER FOUND, ERROR MESSAGE IF ERROR, USER IF SUCCESS
    {
        $result = array();
        $result["status"] = 0;
        $result["content"] = null;
        try{
            $db = Connection::getInstance();
            $req = $db->prepare("SELECT * FROM borrows WHERE equipment_id =:eqp_id AND user_id=:user_id ORDER BY date_submitted DESC");
            $req->execute(array(
                'eqp_id' => $equipmentId,
                'user_id' => $userId
            ));
            $list = array();
            if($req->rowCount() == 0){
                $result["status"] = 0;
                $result["content"] = 'Nothing found.';
                return $result;
            }else{
                foreach ($req->fetchAll() as $br) {
                    $borrow = new Borrow();
                    $borrow->set_id($br['id']);
                    $borrow->set_date_borrow($br['date_borrow']);
                    $borrow->set_date_retrieve($br['date_retrieve']);
                    $borrow->set_date_borrow_check($br['date_borrow_check']);
                    $borrow->set_date_retrieve_check($br['date_retrieve_check']);
                    $eq = Equipment::findEquipmentById($equipmentId)["content"];
                    $borrow->set_equipment_name($eq->get_name());
                    $borrow->set_equipment_image($eq->get_image());
                    $user = User::findUserById($br['user_id'])["content"];
                    $borrow->set_borrower_first_name($user->get_first_name());
                    $borrow->set_borrower_last_name($user->get_last_name());
                    $borrow->set_retrieved($br['retrieved']);
                    $borrow->set_equipment_id($br['equipment_id']);
                    $borrow->set_borrower_id($br['user_id']);
                    array_push($list,$borrow);
                }
                $result["status"] = 1;
                $result["content"] = $list;
                return $result;
            }
        }catch(Exception $e){
            $result["status"] = 0;
            $result["content"] = $e->getMessage();
            return $result;
        }
    }


    public static function confirmBorrowDate($borrowId,$borrowType) //RETURN: 1 IF SUCCESS, ERROR MESSAGE IF NOT
    {
        $result = array();
        $result["status"] = 0;
        $result["content"] = null;
        try{
            $db = Connection::getInstance();
            if($borrowType == "borrowed"){
                $req = $db->prepare("UPDATE borrows SET  date_borrow_check=1 WHERE id=:bId");
                $req->execute(array(
                    'bId' => $borrowId
                ));
                $result["status"] = 1;
                $result["content"] = "good";
                return $result;
            }else{
                $req = $db->prepare("UPDATE borrows SET date_retrieve_check=1, date_borrow_check=1 WHERE id=:bId");
                $req->execute(array(
                    'bId' => $borrowId
                ));
                $result["status"] = 1;
                $result["content"] = "good";
                return $result;
            }
        }catch(Exception $e){
            $result["status"] = 0;
            $result["content"] = $e->getMessage();
            return $result;
        }
    }



}
?>