<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ahmed JH</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap-select.css">

    <style>
        .thumbnail:hover{
            background-color: black;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<?php
include("navbar.php");
?>

<div class="container-fluid">




    <?php
    require_once('Connection.php');
    require_once('Entities/User.php');
    $result = User::getPendingUsers();
    if($result['status'] == 1){
        ?>
        <div class="row">
            <div class="col-md-4 col-md-offset-4" style="padding: 20px">
                <center><h3><strong>Pending Users</strong></h3></center>
                <center><p id="error_message" class="text-danger hidden">Error, please check your email and password.</p></center>
                <center><p id="success_message" class="text-success hidden">Equipment successfully added.</p></center>
                <table class="table table-bordered" >
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    foreach ($result['content'] as $one){
                        ?>
                        <tr>
                            <td><?php echo $one->get_first_name(); ?></td>
                            <td><?php echo $one->get_last_name(); ?></td>
                            <td><?php echo $one->get_email(); ?></td>
                            <td>
                                <a class="btn btn-info" onclick="activate('<?php echo $one->get_id(); ?>','<?php echo $one->get_first_name(); ?>','<?php echo $one->get_last_name(); ?>')"><i class="fa fa-unlock" aria-hidden="true"></i> Activate</a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
        <?php
    }else{
        ?>
        <div class="row">
            <div class="col-md-4 col-md-offset-4" style="padding: 20px">
                <center><h3><strong>Pending Users</strong></h3></center>
                <center><p id="error_message" class="text-danger hidden">Error, please check your email and password.</p></center>
                <center><p id="success_message" class="text-success hidden">Equipment successfully added.</p></center>
                <center style="margin-top: 50px">
                    <h4>No Users Found</h4>
                    <i class="fa fa-refresh fa-spin fa-pulse fa-2x" aria-hidden="true"></i>
                </center>
            </div>
        </div>
        <?php
    }
    ?>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/bootstrap-select.js"></script>


<!-- Button trigger modal -->
<button type="button" id="btn_launch_modal" class="btn btn-primary btn-lg hidden" data-toggle="modal" data-target="#myModal">
    Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Activate User</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to activate the user <strong id="username_activate"></strong> ?
            </div>
            <div class="modal-footer">
                <form method="post" action="Validators/activate_user_validator.php">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input id="user_id_activate" name="user_id_activate" type="hidden" value="0">
                    <input type="submit" value="Activate" class="btn btn-primary">
                </form>

            </div>
        </div>
    </div>
</div>


<script>
    function activate(userId,userFirstName,userLastName){
        $('#username_activate').html(userFirstName + ' ' + userLastName);
        $('#user_id_activate').val(userId);
        $('#btn_launch_modal').click();
    }
</script>



<?php
if(isset($_SESSION["error"])){
    ?>
    <script>
        $('#error_message').html('<?php echo $_SESSION["error"] ?>');
        $('#error_message').removeClass("hidden");
    </script>
    <?php
    $_SESSION["error"] = null;
}
?>

<?php
if(isset($_SESSION["success"])){
    ?>
    <script>
        $('#success_message').html('<?php echo $_SESSION["success"] ?>');
        $('#success_message').removeClass("hidden");
    </script>
    <?php
    $_SESSION["success"] = null;
}
?>



</body>
</html>