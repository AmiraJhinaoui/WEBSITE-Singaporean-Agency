<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Equipments</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap-select.css">

    <style>
        .thumbnail:hover{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<?php
include("navbar.php");
?>



<?php
if(isset($_GET['page'])){
    $numPage = $_GET['page'];
}else{
    $numPage = 1;
}
?>

<div class="container-fluid">

    <div class="row">
        <div class="col-lg-3">

            <form class="form-inline">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" id="search_input" onkeyup="searchEquipment()" placeholder="Search equipment...">
                        <div class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></div>
                    </div>
                </div>
            </form>

            <br>




            <?php
            require_once('Connection.php');
            require_once('Entities/Equipment.php');
            require_once('Entities/User.php');
            require_once('Entities/Borrow.php');
            ?>


        </div>
        <div class="col-lg-9">

            <?php
            $result = Equipment::getAllEquipmentsPagination(($numPage-1)*8,8);
            if($result['status'] == 1){
                ?>
                <div class="row" id="div_all_equipments">
                    <?php
                    $list = $result["content"];
                    foreach ($list as $one){
                        ?>
                        <div class="col-sm-6 col-xs-12 col-md-3 element" >
                            <div class="thumbnail hoverable" style="border-radius: 0px; ">
                                <p class="hidden element-name"><?php echo $one->get_name(); ?></p>
                                <br>
                                <img class="img-responsive img-circle" src="<?php echo $one->get_image() ?>" alt="..." style="width: 150px; height: 150px">
                                <div class="caption">
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: center;font-family: 'Montserrat', sans-serif;">
                                            <strong><h5 data-toggle="tooltip" data-placement="top" title="<?php echo $one->get_name(); ?>"><?php if(strlen($one->get_name()) > 25){echo substr($one->get_name(),0,25) . '...';}else{echo $one->get_name();} ?></h5></strong>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                    <p style="text-align: center">
                                        <a href="borrow.php?equipment=<?php echo $one->get_serial_number() ?>" class="btn  btn-sm" style="border-radius: 0px">
                                            <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                                            Borrow
                                        </a>
                                    </p>
                                    <?php
                                    if($_SESSION['login']['role'] == "admin"){
                                        ?>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6" style="text-align: center">
                                                <a style="border-radius: 0px" href="update_equipment.php?equipment=<?php echo $one->get_serial_number() ?>" class="btn btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Update</a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6 pull-right" style="text-align: center">
                                                <a style="border-radius: 0px" href="#" onclick="deleteEquipment('<?php echo $one->get_serial_number() ?>','<?php echo $one->get_name() ?>','<?php echo $one->get_image() ?>')" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                                    Delete</a>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>

                <div class="row" style="text-align: center">
                    <nav aria-label="Page navigation pull-left">
                        <ul class="pagination">
                            <li style="<?php if($numPage == 1) echo 'pointer-events: none' ?>" >
                                <a href="equipments.php?page=<?php echo $numPage-1 ?>" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <?php
                            for($i = 1; $i <= ceil($result['count']/8); $i++){
                                ?>
                                <li class="<?php if($i == $numPage) echo 'active' ?>"><a href="equipments.php?page=<?php echo $i ?>"><?php echo $i ?></a></li>
                                <?php
                            }
                            ?>
                            <!--li class="active"><a href="#">1</a></li-->
                            <li style="<?php if($numPage == ceil($result['count']/8)) echo 'pointer-events: none' ?>">
                                <a href="equipments.php?page=<?php echo $numPage+1 ?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <?php
            }else{
                ?>
                <center style="margin-top: 50px; margin-right: 150px;">
                    <h4>No Equipments Found</h4>
                    <i class="fa fa-refresh fa-spin fa-pulse fa-2x" aria-hidden="true"></i>
                </center>
                <?php
            }
            ?>

        </div>
    </div>
</div>







<a class="hidden" data-toggle="modal" data-target="#modal_delete_equipment" id="btn_modal_delet_eq"></a>


<!-- Modal -->
<div class="modal fade" id="modal_delete_equipment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Delete Equipment</h4>
            </div>

            <form class="form-horizontal" method="post" action="Validators/delete_equipment_validator.php">
                <div class="modal-body">

                    Are you sure you want to delete <strong id="eq_name"></strong>

                    <input type="hidden" name="eq_serial" id="eq_serial">
                    <input type="hidden" name="eq_img" id="eq_img">
                </div>
                <div class="modal-footer">

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                 Delete</button>
                        </div>
                    </div>
                </div>
            </form>


        </div>
    </div>
</div>









<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/bootstrap-select.js"></script>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

</script>

<script>
    function searchEquipment(){
        var input, filter, ul, li, a, i;
        input = $('#search_input').val();
        filter = input.toUpperCase();
        ul = document.getElementById("div_all_equipments");
        li = ul.getElementsByClassName("element");

        for (i = 0; i < li.length; i++){
            a = li[i].getElementsByClassName("element-name")[0];
            console.log(a);
            if(a.innerHTML.toUpperCase().indexOf(filter) > -1){
                li[i].style.display = "";
            }else{
                li[i].style.display = "none";
            }
        }
    }

    function deleteEquipment(serial,name,img) {
        $('#eq_serial').val(serial);
        $('#eq_img').val(img);
        $('#eq_name').html(name);
        $('#eq_name').html(name);
        $('#btn_modal_delet_eq').click();
    }

</script>



<?php
if(isset($_SESSION["error"])){
    ?>
    <script>
        //alert('<?php echo $_SESSION["error"] ?>');
        swal(
            'ERROR!',
            '<?php echo $_SESSION["success"] ?>',
            'error'
        )
    </script>
    <?php
    $_SESSION["error"] = null;
}
?>

<?php
if(isset($_SESSION["success"])){
    ?>
    <script>
        //alert('<?php echo $_SESSION["success"] ?>');
        swal(
            'SUCCESS!',
            '<?php echo $_SESSION["success"] ?>',
            'success'
        )
    </script>
    <?php
    $_SESSION["success"] = null;
}
?>


</body>
</html>