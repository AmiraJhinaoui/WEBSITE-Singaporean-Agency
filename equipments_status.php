<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ahmed JH</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap-select.css">

    <style>
        .thumbnail:hover{
            background-color: black;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<?php
include("navbar.php");
?>

<div class="container-fluid">


    <?php
    require_once('Connection.php');
    require_once('Entities/User.php');
    require_once('Entities/Equipment.php');
    require_once('Entities/Borrow.php');
    ?>



    <div class="row">
        <center><h3><strong>Equipments Status</strong></h3></center>
        <div class="col-md-12" style="padding: 20px">
            <table class="table table-bordered">
                <tr>
                    <th style="width: 5%"></th>
                    <th style="width: 10%">Equipment name</th>
                    <th style="width: 10%">Equipment Serial number</th>
                    <th style="width: 10%">Equipment PO number</th>
                    <th style="width: 10%">Equipment ERP number</th>
                    <th style="width: 55%">Borrow request</th>
                </tr>
                <?php
                $allEqpsResult = Equipment::getAllEquipments();
                if($allEqpsResult['status'] == 1){
                    foreach ($allEqpsResult['content'] as $oneEqp){
                        $resultBorrow = Borrow::getBorrowDetails($oneEqp->get_serial_number());
                        if($resultBorrow['status'] == 1){
                            ?>
                            <tr>
                                <td><img src="<?php echo $oneEqp->get_image() ?>" class="img-circle" alt="..." style="width:50px;height:50px;  "></td>
                                <td><?php echo $oneEqp->get_name() ?></td>
                                <td><?php echo $oneEqp->get_serial_number() ?></td>
                                <td><?php echo $oneEqp->get_po_number() ?></td>
                                <td><?php echo $oneEqp->get_erp_number() ?></td>
                                <td>
                                    <table class="table table-condensed ">
                                        <thead>
                                        <tr>
                                            <th>Borrower Name</th>
                                            <th>Date Borrow</th>
                                            <th>Date Retrieve</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                            foreach ($resultBorrow['content'] as $oneBr){
                                ?>
                                <tr>
                                    <td><?php echo $oneBr->get_borrower_first_name() ?> <?php echo $oneBr->get_borrower_last_name() ?></td>
                                    <td>
                                        <?php echo $oneBr->get_date_borrow() ?>
                                        <?php
                                        if($oneBr->get_date_borrow_check() == 0){
                                            echo '<i style="color: red"  class="fa fa-times-circle-o" aria-hidden="true"></i>';
                                        }else{
                                            echo '<i style="color: green" class="fa fa-check-circle-o" aria-hidden="true"></i>';
                                        }
                                        ?>
                                        <a class="btn btn-info btn-sm <?php if($oneBr->get_date_borrow_check() == 1) echo "disabled" ?>" onclick="confirmDate('<?php echo $oneBr->get_id() ?>','<?php echo $oneEqp->get_name() ?>','<?php echo $oneBr->get_date_borrow() ?>','borrowed','<?php echo $oneBr->get_borrower_first_name() ?>','<?php echo $oneBr->get_borrower_last_name() ?>')">Confirm</a>
                                    </td>
                                    <td>
                                        <?php echo $oneBr->get_date_retrieve() ?>
                                        <?php
                                        if($oneBr->get_date_retrieve_check() == 0){
                                            echo '<i style="color: red"  class="fa fa-times-circle-o" aria-hidden="true"></i>';
                                        }else{
                                            echo '<i style="color: green" class="fa fa-check-circle-o" aria-hidden="true"></i>';
                                        }
                                        ?>
                                        <a class="btn btn-info btn-sm <?php if($oneBr->get_date_retrieve_check() == 1) echo "disabled" ?>" onclick="confirmDate('<?php echo $oneBr->get_id() ?>','<?php echo $oneEqp->get_name() ?>','<?php echo $oneBr->get_date_borrow() ?>','retrieved','<?php echo $oneBr->get_borrower_first_name() ?>','<?php echo $oneBr->get_borrower_last_name() ?>')">Confirm</a>
                                    </td>
                                </tr>
                                <?php
                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                }
                ?>
            </table>
        </div>
    </div>
</div>




<!-- Button trigger modal -->
<button id="btn_launch_modal_confirm" type="button" class="btn btn-primary btn-lg hidden" data-toggle="modal" data-target="#modal_confirm">
    modal
</button>

<!-- Modal -->
<div class="modal fade" id="modal_confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirm ?</h4>
            </div>

            <form class="form-horizontal" method="post" action="Validators/confirm_date_validator.php">
                <div class="modal-body">
                    Are you sure you want to confirm that <strong id="modal_username">username</strong> have <strong id="modal_type">taken</strong> <strong id="modal_equipment">aqp</strong> on the <strong id="modal_date">26/08/2017</strong>?

                    <input id="modal_borrow_id" type="hidden" value="0" name="borrow_id">
                    <input id="modal_borrow_type" type="hidden" value="0" name="borrow_type">
                </div>
                <div class="modal-footer">

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Sure</button>
                        </div>
                    </div>
                </div>
            </form>


        </div>
    </div>
</div>










<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/bootstrap-select.js"></script>


<?php
if(isset($_SESSION["error"])){
    ?>
    <script>
        $('#error_message').html('<?php echo $_SESSION["error"] ?>');
        $('#error_message').removeClass("hidden");
    </script>
    <?php
    $_SESSION["error"] = null;
}
?>

<?php
if(isset($_SESSION["success"])){
    ?>
    <script>
        $('#success_message').html('<?php echo $_SESSION["success"] ?>');
        $('#success_message').removeClass("hidden");
    </script>
    <?php
    $_SESSION["success"] = null;
}
?>

<script>
    function confirmDate(borrowId,eqpName,date,type,userFName,userLName) {
        $('#modal_username').html(userFName + ' ' + userLName);
        $('#modal_type').html(type);
        $('#modal_equipment').html(eqpName);
        $('#modal_date').html(date);
        $('#modal_borrow_id').val(borrowId);
        $('#modal_borrow_type').val(type);
        $('#btn_launch_modal_confirm').click();
    }
</script>




</body>
</html>