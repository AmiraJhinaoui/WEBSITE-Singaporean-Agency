<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ahmed JH</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap-select.css">

    <style>
        .thumbnail:hover{
            background-color: black;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<?php
include("navbar.php");
?>

<div class="container-fluid">


    <?php
    require_once('Connection.php');
    require_once('Entities/User.php');
    require_once('Entities/Equipment.php');
    require_once('Entities/Borrow.php');
    ?>



    <div class="row">
        <center><h3><strong>My Borrows</strong></h3></center>
        <div class="col-md-12" style="padding: 20px">
            <table class="table table-bordered">
                <tr>
                    <th style="width: 5%"></th>
                    <th style="width: 10%">Equipment name</th>
                    <th style="width: 55%">Borrows</th>
                </tr>
                <?php
                $allEqpsResult = Equipment::getAllEquipments();
                if($allEqpsResult['status'] == 1){
                    foreach ($allEqpsResult['content'] as $oneEqp){
                        $resultBorrow = Borrow::getUserBorrows($_SESSION['login']['id'],$oneEqp->get_serial_number());
                        if($resultBorrow['status'] == 1){
                            ?>
                            <tr>
                                <td><img src="<?php echo $oneEqp->get_image() ?>" class="img-circle" alt="..." style="width:50px;height:50px;  "></td>
                                <td><?php echo $oneEqp->get_name() ?></td>
                                <td>
                                    <table class="table table-condensed ">
                                        <thead>
                                        <tr>
                                            <th>Date Borrow</th>
                                            <th>Date Retrieve</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($resultBorrow['content'] as $oneBr){
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo $oneBr->get_date_borrow() ?>
                                                    <?php
                                                    if($oneBr->get_date_borrow_check() == 0){
                                                        echo '<i style="color: red"  class="fa fa-times-circle-o" aria-hidden="true"></i>';
                                                    }else{
                                                        echo '<i style="color: green" class="fa fa-check-circle-o" aria-hidden="true"></i>';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php echo $oneBr->get_date_retrieve() ?>
                                                    <?php
                                                    if($oneBr->get_date_retrieve_check() == 0){
                                                        echo '<i style="color: red"  class="fa fa-times-circle-o" aria-hidden="true"></i>';
                                                    }else{
                                                        echo '<i style="color: green" class="fa fa-check-circle-o" aria-hidden="true"></i>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                }
                ?>
            </table>
        </div>
    </div>
</div>










<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/bootstrap-select.js"></script>


<?php
if(isset($_SESSION["error"])){
    ?>
    <script>
        $('#error_message').html('<?php echo $_SESSION["error"] ?>');
        $('#error_message').removeClass("hidden");
    </script>
    <?php
    $_SESSION["error"] = null;
}
?>

<?php
if(isset($_SESSION["success"])){
    ?>
    <script>
        $('#success_message').html('<?php echo $_SESSION["success"] ?>');
        $('#success_message').removeClass("hidden");
    </script>
    <?php
    $_SESSION["success"] = null;
}
?>




</body>
</html>