<?php
session_start();
if(isset($_SESSION['login'])){
    ?>
    <script>window.location = 'equipments.php';</script>
    <?php
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ahmed JH</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="css/font-awesome.min.css" rel="stylesheet">

    <style>
        .thumbnail:hover{
            background-color: black;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->



    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <style>
        body{
            background: url("img/21.jpg") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        h1, label{
            color:#0000A0;
        }
        .artc_logout
        {
            text-align:right;
            padding-right:50px;
            padding-top:10px;
        }
    </style>
</head>
<body>



<div style="background-color: #94DE6E;">
    <div class="artc_header">

        <div class="container">
            <br>
            <div class="artc_logo col-md-3 col-sm-3 col-xs-3">
                <a id="dnn_artc_header_artc_Logo_hypLogo" title="Advanced Remanufacturing and Technology Centre (ARTC)" href="https://www.a-star.edu.sg/artc/"><img class="img-responsive" id="dnn_artc_header_artc_Logo_imgLogo" src="img/artc_new_logo.png" alt="Advanced Remanufacturing and Technology Centre (ARTC)"></a>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-6"></div>
            <div class="col-md-3 col-sm-3 col-xs-3 nanyang_logo" style="text-align:right">

                <a href="http://www.ntu.edu.sg/"><img src="img/nanyang_logo1.png" class="nanyang-img img-responsive"></a>
            </div>


        </div>
    </div>
</div>



<nav class="navbar navbar-inverse" role="navigation" style="font-family: 'Montserrat', sans-serif; background-color: #000000; border-radius: 0px" >
    <div class="container">
        <div class="navbar-header " >
            <a class="navbar-brand" href="index.php" id="ddr"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> <strong>HOME</strong></a>
            <a class="navbar-brand navbar-left" href="team.php" id="ddh"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <strong>OUR TEAM</strong></a>
        </div>
    </div><!-- /.container-fluid -->
</nav>






<div class="container-fluid">



    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4" style="padding: 20px">
                <center><h3><strong>Login</strong></h3></center>
                <p id="error_message" class="text-danger hidden">Error, please check your email and password.</p>
                <p id="success_message" class="text-success hidden">.</p>
                <form method="post" action="Validators/login_validator.php">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                        <p class="text-danger hidden">Example block-level help text here.</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                        <p class="text-danger hidden">Example block-level help text here.</p>
                    </div>
                    <!--div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember_me"> Remember Me
                        </label>
                    </div-->
                    <button type="submit" class="btn btn-primary">Login</button>
                    <a href="register.php" class="pull-right">New ? Register here</a>
                </form>
            </div>
        </div>
    </div>
</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script src="js/bootstrap-datepicker.min.js"></script>


<?php
if(isset($_SESSION["error"])){
    ?>
    <script>
        $('#error_message').html('<?php echo $_SESSION["error"] ?>');
        $('#error_message').removeClass("hidden");
    </script>
    <?php
    $_SESSION["error"] = null;
}
?>


<?php
if(isset($_SESSION["success"])){
    ?>
    <script>
        $('#success_message').html('<?php echo $_SESSION["success"] ?>');
        $('#success_message').removeClass("hidden");
    </script>
    <?php
    $_SESSION["success"] = null;
}
?>


</body>
</html>