<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ahmed JH</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap-select.css">

    <style>
        .thumbnail:hover{
            background-color: black;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<?php
include("navbar.php");
?>

<div class="container-fluid">




    <div class="row">
        <center><p id="error_message" class="text-danger hidden">Error, please check your email and password.</p></center>
        <center><p id="success_message" class="text-success hidden">Equipment successfully added.</p></center>




        <div class="col-md-8 col-md-offset-2 " style="padding: 20px">

            <div class="row">
                <div class="col-md-6 ">
                    <div class="parent" style="position: relative;top: 0; left: 0;" onmouseover="hoverImage()" onmouseleave="leaveImage()">
                        <img id="user_img"  src="<?php echo $_SESSION['login']['image'] ?>" class="img-rounded img-responsive" style="position: relative;top: 0;left: 0; display: block;margin-left: auto;margin-right: auto; border-radius: 150px; width: 300px; height: 300px">
                        <a href="#" onclick="chooseImg()" style="color: inherit" ><i id="user_change_img" class="fa fa-camera fa-5x hidden" aria-hidden="true" style="position: absolute;top: 40%;left: 40%;"></i></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <br>
                    <br>
                    <i class="fa fa-user-circle-o fa-2x" aria-hidden="true"> <?php echo $_SESSION['login']['first_name'] ?> <?php echo $_SESSION['login']['last_name'] ?></i>
                    <a class="pull-right" data-toggle="modal" data-target="#modal_change_name">change</a>
                    <br>
                    <hr>
                    <br>
                    <i class="fa fa-envelope-o fa-2x" aria-hidden="true"> <?php echo $_SESSION['login']['email'] ?></i>
                    <a class="pull-right" data-toggle="modal" data-target="#modal_change_email">change</a>
                    <br>
                    <hr>
                    <br>
                    <i class="fa fa-lock fa-2x" aria-hidden="true"> *********</i>
                    <a class="pull-right" data-toggle="modal" data-target="#modal_change_password">change</a>
                </div>
            </div>

        </div>
    </div>
</div>

<form id="form_change_profile_image" method="post" action="Validators/change_image_validator.php"  enctype="multipart/form-data">
    <input type="file" id="change_img_file" name="change_img_file" class="hidden">
    <input type="hidden" value="<?php echo $_SESSION['login']['id'] ?>" name="user_id">
</form>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/bootstrap-select.js"></script>


<?php
if(isset($_SESSION["error"])){
    ?>
    <script>
        $('#error_message').html('<?php echo $_SESSION["error"] ?>');
        $('#error_message').removeClass("hidden");
    </script>
    <?php
    $_SESSION["error"] = null;
}
?>

<?php
if(isset($_SESSION["success"])){
    ?>
    <script>
        $('#success_message').html('<?php echo $_SESSION["success"] ?>');
        $('#success_message').removeClass("hidden");
    </script>
    <?php
    $_SESSION["success"] = null;
}
?>


<script>
    function hoverImage(){
        $('#user_change_img').removeClass('hidden');
        $('#user_img').css('filter', 'blur(3px)');
    }

    function leaveImage(){
        $('#user_change_img').addClass('hidden');
        $('#user_img').css('filter', 'blur(0px)');
    }

    function chooseImg(){
        $('#change_img_file').click();
    }

    $('#change_img_file').on('change', function(){
        input = document.getElementById('change_img_file');
        file = input.files[0];
        console.log(file);

        fr = new FileReader();
        fr.onload = showImage;
        //fr.readAsText(file);
        fr.readAsDataURL(file);

        function showImage() {
            $("#user_img").attr("src",fr.result);
            $('#form_change_profile_image').submit();
        }
    })

</script>





<!-- Modal -->
<div class="modal fade" id="modal_change_name" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Change Name</h4>
            </div>

            <form class="form-horizontal" method="post" action="Validators/change_name_validator.php">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">First Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="first_name" placeholder="First Name" value=" <?php echo $_SESSION['login']['first_name'] ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Last Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="last_name" placeholder="Last Name" value=" <?php echo $_SESSION['login']['last_name'] ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-4 control-label">Enter your password</label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>

                    <input type="hidden" value="<?php echo $_SESSION['login']['id'] ?>" name="user_id">
                </div>
                <div class="modal-footer">

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                    </div>
                </div>
            </form>


        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal_change_email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Change Email</h4>
            </div>

            <form class="form-horizontal" method="post" action="Validators/change_email_validator.php">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">New Email</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="email" placeholder="Email" value=" <?php echo $_SESSION['login']['email'] ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-4 control-label">Enter your password</label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>

                    <input type="hidden" value="<?php echo $_SESSION['login']['id'] ?>" name="user_id">
                </div>
                <div class="modal-footer">

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                    </div>
                </div>
            </form>


        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Change Password</h4>
            </div>

            <form class="form-horizontal" method="post" action="Validators/change_password_validator.php" id="form_change_password">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Old Password</label>
                        <div class="col-sm-7">
                            <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Old Password"  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">New Password</label>
                        <div class="col-sm-7">
                            <input type="password" class="form-control " id="new_password" name="new_password" placeholder="New Password (at least 8 caracters)" required>
                            <p id="error_message_password_1" class="text-danger hidden">Password dosen't match.</p>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Confirm Password</label>
                        <div class="col-sm-7">
                            <input type="password" class="form-control " id="confirm_password" name="confirm_password" placeholder="Confirm Password" required>
                            <p id="error_message_password_2" class="text-danger hidden">Password dosen't match.</p>
                        </div>
                    </div>

                    <input type="hidden" value="<?php echo $_SESSION['login']['id'] ?>" name="user_id">
                </div>
                <div class="modal-footer">

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" onclick="checkPassword($('#new_password').val(),$('#confirm_password').val())">Save Changes</button>
                            <input type="submit" class="hidden" id="btn_submit_change_pass">
                        </div>
                    </div>
                </div>
            </form>


        </div>
    </div>
</div>


<script>
    function checkPassword(pass1,pass2) {
        if(pass1.localeCompare(pass2) == 0){
            $('#new_password').removeClass('list-group-item-danger');
            $('#confirm_password').removeClass('list-group-item-danger');
            $('#error_message_password_1').addClass('hidden');
            $('#error_message_password_2').addClass('hidden');
            $('#btn_submit_change_pass').click();
            //$('#form_change_password').submit();
            /*if($('#old_password').val() == ''){
                alert('emptyy');
            }*/
        }else{
            $('#new_password').addClass('list-group-item-danger');
            $('#confirm_password').addClass('list-group-item-danger');
            $('#error_message_password_1').removeClass('hidden');
            $('#error_message_password_2').removeClass('hidden');
        }
    }
</script>




</body>
</html>